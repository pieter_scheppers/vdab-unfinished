<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTaskTags extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('task_tags', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('tag_id');
			$table->unsignedInteger('task_id');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Schema::dropIfExists('task_tags');
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');
	}

}
