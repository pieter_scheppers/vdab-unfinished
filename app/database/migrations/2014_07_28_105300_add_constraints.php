<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConstraints extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('tasks_req', function(Blueprint $table) {
            $table->foreign('req_id')->references('id')->on('tasks');
            $table->foreign('task_id')->references('id')->on('tasks');
        });
		Schema::table('task_tags', function(Blueprint $table) {
			$table->foreign('tag_id')->references('id')->on('tags');
			$table->foreign('task_id')->references('id')->on('tasks');			
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('tasks_req', function(Blueprint $table) {
            DB::statement('SET FOREIGN_KEY_CHECKS = 0');
			Schema::dropIfExists('tasks_req');
			DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        });
		Schema::table('task_tags', function(Blueprint $table) {
            DB::statement('SET FOREIGN_KEY_CHECKS = 0');
			Schema::dropIfExists('task_tags');
			DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        });
    }
}
