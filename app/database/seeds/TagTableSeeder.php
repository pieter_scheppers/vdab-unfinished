<?php

class TagTableSeeder extends Seeder {

    public function run() {
        DB::table('tags')->delete();
        $this->createtTag('student');
        $this->createtTag('secetariaat');
        $this->createtTag('jobstudent');
        $this->createtTag('verkoop');
        $this->createtTag('account');
        $this->createtTag('cv');
        $this->createtTag('opleiding');
    }

    protected function createtTag($name) {
        $tag = new Tag(
                array(
            'name' => $name
                )
        );
        $tag->save();
    }

}
