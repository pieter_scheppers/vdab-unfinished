<?php

class TaskTagsTableSeeder extends Seeder {

    public function run() {
        DB::table('task_tags')->delete();

     /*   $this->createTaskTag(1,1);
        $this->createTaskTag(1,3);
        $this->createTaskTag(2,1);
        $this->createTaskTag(2,3);
		$this->createTaskTag(4,4);
		$this->createTaskTag(4,3);
		$this->createTaskTag(4,3);*/
    }

    protected function createTaskTag($tag_id,$task_id) {
        $Task_tags = new TaskTags(
                array(
            'tag_id' => $tag_id,
			'task_id'=> $task_id
                )
        );
		$Task_tags->timestamps = false;
        $Task_tags->save();
    }

}
