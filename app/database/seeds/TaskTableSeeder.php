<?php

class TaskTableSeeder extends Seeder {

    public function run() {
        DB::table('tasks')->delete();

        $this->createTask('CV Maken', 'U kan nu uw eigen cv maken.', 30);
        $this->createTask('Personaliseren', 'Pas uw persoonlijke informatie aan', 15);
        $this->createTask('Aanmoediging krijgen', 'Krijg aanmoediging via sms of per e-mail', 15);
        $this->createTask('Banen oplijsten', 'Geef uw vorige werkervaringen op', 30);
        $this->createTask('Vrienden vertellen', 'Nodig uw vrienden uit om deze app te gebruiken', 15);
    }

    protected function createTask($title, $desc, $xp) {
        $task = new Task(
                array(
            'title' => $title,
            'description' => $desc,
            'xp' => $xp
                )
        );
        $task->save();
    }

}
