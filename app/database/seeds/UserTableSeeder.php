<?php

class UserTableSeeder extends Seeder {

    public function run() {
        Eloquent::unguard();

        User::create(
                array(
                    'first_name' => 'Jan',
                    'last_name' => 'Oris',
                    'email' => 'jan.oris@gmail.com',
                    'password' => Hash::make('password'),
                )
        );

        User::create(
                array(
                    'first_name' => 'Andreas',
                    'last_name' => 'Creten',
                    'email' => 'andreas@madewithlove.be',
                    'password' => Hash::make('password'),
                )
        );

        User::create(
                array(
                    'first_name' => 'John',
                    'last_name' => 'Doe',
                    'email' => 'john.doe@server.com',
                    'password' => Hash::make('password'),
                )
        );
    }

}
