<?php

class RelationshipSeeder extends Seeder {

    public function run() {
        DB::table('tasks_req')->delete();

        //set required tasks
        $task1 = Task::find(1);
        $task2 = Task::find(2);
        $task3 = Task::find(3);
        $task4 = Task::find(4);
        $task5 = Task::find(5);

        $tag1 = Tag::find(1);
        $tag2 = Tag::find(2);
        $tag3 = Tag::find(3);
        $tag4 = Tag::find(4);
        $tag5 = Tag::find(5);
        $tag6 = Tag::find(6);
        $tag7 = Tag::find(7);

        $task2->requiredTasks()->attach($task1->id);
        $task3->requiredTasks()->attach($task1->id);
        $task3->requiredTasks()->attach($task2->id);
        $task4->isRequiredBy()->attach($task3->id);
        $task5->isRequiredBy()->attach($task1->id);
        $task5->isRequiredBy()->attach($task4->id);

        $task1->tags()->attach($tag1->id);
        $task1->tags()->attach($tag7->id);
        $task1->tags()->attach($tag6->id);
        $task1->tags()->attach($tag2->id);
        $task2->tags()->attach($tag2->id);
        $task2->tags()->attach($tag5->id);
        $task3->tags()->attach($tag2->id);
        $task3->tags()->attach($tag1->id);
        $task4->tags()->attach($tag6->id);
        $task4->tags()->attach($tag5->id);
        $task4->tags()->attach($tag2->id);
        $task5->tags()->attach($tag4->id);
        $task5->tags()->attach($tag5->id);
    }

}
