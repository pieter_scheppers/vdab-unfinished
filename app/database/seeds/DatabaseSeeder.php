<?php

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Eloquent::unguard();

        $this->call('UserTableSeeder');
        $this->call('TasktableSeeder');
        $this->call('TaskStepSeeder');
        $this->call('TagTableSeeder');
        $this->call('TaskTagsTableSeeder');
        $this->call('RelationshipSeeder');
    }

}
