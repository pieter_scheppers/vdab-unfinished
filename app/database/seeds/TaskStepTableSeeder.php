<?php

class TaskStepSeeder extends Seeder {

    public function run() {
        DB::table('task_steps')->delete();
        DB::table('step_questions')->delete();
        DB::table('step_texts')->delete();

        //task 1
        $this->createQuestion(1, 4, 'Welke Diploma\'s heeft u?', 'ASO, BSO, TSO, BA, MA');
        $this->createQuestion(1, 3, 'Wat is uw Geslacht?', 'M, V');
        $this->createText(1, 3, 'Pangram', 'The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz, vex nymphs. Waltz, bad nymph, for quick jigs vex! Fox nymphs grab quick-jived waltz. Brick quiz whangs jumpy veldt fox. ');

        //task2
        $this->createText(2, 2, 'Kafka', 'One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections.');
        $this->createQuestion(2, 5, 'Pangram', 'The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz, vex nymphs. Waltz, bad nymph, for quick jigs vex! Fox nymphs grab quick-jived waltz. Brick quiz whangs jumpy veldt fox. ');

        //task3
        $this->createText(3, 1, 'Kafka', 'One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections.');
        $this->createText(3, 2, 'Kafka', 'One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections.');
        $this->createQuestion(3, 2, 'Pangram', 'The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz, vex nymphs. Waltz, bad nymph, for quick jigs vex! Fox nymphs grab quick-jived waltz. Brick quiz whangs jumpy veldt fox. ');

        //task4
        $this->createText(4, 2, 'Kafka', 'One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections.');
        $this->createQuestion(4, 1, 'Pangram', 'The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz, vex nymphs. Waltz, bad nymph, for quick jigs vex! Fox nymphs grab quick-jived waltz. Brick quiz whangs jumpy veldt fox. ');
        $this->createQuestion(4, 1, 'Pangram', 'The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz, vex nymphs. Waltz, bad nymph, for quick jigs vex! Fox nymphs grab quick-jived waltz. Brick quiz whangs jumpy veldt fox. ');

        //task5
        $this->createText(5, 4, 'Kafka', 'One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections.');
        $this->createText(5, 3, 'Kafka', 'One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections.');
        $this->createText(5, 3, 'Kafka', 'One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections.');
    }

    protected function createTaskStep($task_id, $xp_weight, $type) {
        $step = new TaskStep(
                array(
            'task_id' => $task_id,
            'xp_weight' => $xp_weight,
            'type' => $type
                )
        );
        $step->save();
        return $step;
    }

    protected function createQuestion($task_id, $xp_weight, $question, $answer) {
        $step_id = $this->createTaskStep($task_id, $xp_weight, 'StepQuestion')->id;
        $q = new StepQuestion(
                array(
            'task_step_id' => $step_id,
            'question' => $question,
            'answer' => $answer
                )
        );
        $q->save();
    }

    protected function createText($task_id, $xp_weight, $title, $text) {
        $step_id = $this->createTaskStep($task_id, $xp_weight, 'StepText')->id;
        $t = new StepText(
                array(
            'task_step_id' => $step_id,
            'title' => $title,
            'text' => $text
                )
        );
        $t->save();
    }

}
