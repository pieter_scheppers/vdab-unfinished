<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */

Route::get('/login', 'AuthenticationController@showLogin');
Route::post('/login', 'AuthenticationController@processLogin');
Route::get('/logout', 'AuthenticationController@logout');

Route::get('/', function() {
    return Redirect::to('/tasks');
});

Route::group(array('before' => 'auth'), function() {
    Route::resource('/tasks', 'TaskController');
    Route::resource('/tags', 'TagController');
    Route::resource('/users', 'UserController');
});

Route::api(['version' => 'v1'], function() {
    // Route::resource('users', 'UserController');
});

Route::api(['version' => 'v2', 'prefix' => 'api'], function() {
    Route::resource('users', 'UserApiV2Controller');
});
