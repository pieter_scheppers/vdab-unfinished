<?php

class EloquentTaskRepository extends AbstractRepository implements TaskRepositoryInterface {

    protected $modelClassName = 'Task';

    public function findTaskByTitle($title) {
        return Task::where('title', $title)->first();
    }

    public function getRequiredTasks($id) {
        $task = Task::find($id);
        return $task->requiredTasks;
    }

    public function getSteps($id) {
        $task = Task::find($id);
        return $task->steps;
    }

    public function update($id, $data) {
        $task = Task::find($id);
        DB::beginTransaction();
        try {
            $task->title = $data['title'];
            $task->xp = $data['xp'];
            $task->description = $data['description'];
            $task->save();
            $task->tags()->detach();
            $task->requiredTasks()->detach();
            foreach ($task->steps as $step)
                $step->delete();

            if (isset($data['tasks']))
                $this->attachRequiredTasks($data['tasks'], $task);

            if (isset($data['tags'])) {
                $tags = explode(",", $data['tags']);
                $this->addTags($tags, $task);
            }
            if (isset($data['step'])) {
                $steps = $data['step'];
                $this->createsteps($steps, $task->id);
            }

            DB::commit();
            return true;
        } catch (Exception $ex) {
            DB::rollback();
            echo $ex;
        }
        return false;
    }

    public function create(array $data) {
        DB::beginTransaction();
        $success = false;
        try {
            $task = new Task(
                    array(
                'title' => $data['title'],
                'description' => $data['description'],
                'xp' => $data['xp']
                    )
            );
            $task->save();
            $taskid = $task->id;


            if (isset($data['tasks']))
                $this->attachRequiredTasks($data['tasks'], $task);

            if (isset($data['tags'])) {
                $tags = explode(",", $data['tags']);
                $this->addTags($tags, $task);
            }

            if (isset($data['step'])) {
                $steps = $data['step'];
                $this->createsteps($steps, $taskid);
            }
            DB::commit();
            return true;
        } catch (Exception $ex) {
            DB::rollback();
            echo $ex;
        }
        return false;
    }

    protected function addTags($tags, $task) {
        try {
            echo 'addtags';
            $tagrepo = App::make('EloquentTagRepository');
            foreach ($tags as $tagname) {
                $tag = $tagrepo->findTagByName($tagname);

                if ($tag == null) {
                    $tag = new Tag(
                            array(
                        'name' => $tagname
                            )
                    );
                    $tag->save();
                }
                $task->tags()->attach($tag->id);
            }
        } catch (Exception $ex) {
            throw New Exception($ex);
        }
    }

    protected function attachRequiredTasks($requiredTasks, $task) {
        var_dump($requiredTasks);
        try {
            foreach ($requiredTasks as $reqTitle) {
                $reqTask = $this->findTaskByTitle($reqTitle);
                $reqid = $reqTask->id;
                $task->requiredTasks()->attach($reqid);
            }
        } catch (Exception $ex) {
            throw New Exception($ex);
        }
    }

    protected function createSteps($steparray, $taskid) {
        foreach ($steparray as $stepdata) {

            switch ($stepdata['choice']) {
                case 'read':
                    $stepid = $this->createTaskStep($taskid, $stepdata['step_weight'], 'StepText')->id;
                    $text = new StepText(
                            array(
                        'task_step_id' => $stepid,
                        'title' => $stepdata['read_title'],
                        'text' => $stepdata['text_to_read']
                            )
                    );
                    $text->save();
                    break;
                case 'question';
                    $stepid = $this->createTaskStep($taskid, $stepdata['step_weight'], 'StepQuestion')->id;
                    $question = new StepQuestion(
                            array(
                        'task_step_id' => $stepid,
                        'question' => $stepdata['question_title'],
                        'answer' => $stepdata['question_answer']
                            )
                    );
                    $question->save();
                    break;
                default:
                    throw New Exception('no step type');
            }

            return true;
        }
    }

    protected function createTaskStep($task_id, $xp_weight, $type) {
        $step = new TaskStep(
                array(
            'task_id' => $task_id,
            'xp_weight' => $xp_weight,
            'type' => $type
                )
        );
        $step->save();
        return $step;
    }

}
