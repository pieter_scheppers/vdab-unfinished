<?php

use User;

class EloquentUserRepository extends AbstractRepository implements UserRepositoryInterface {

    protected $modelClassName = 'User';

    public function findUserByEmail($email) {
        $where = call_user_func_array("{$this->modelClassName}::where", array('email', $email));
        return $where->get();
        //return User::where('email', $email)->first();
    }

    public function create(array $data) {
        DB::beginTransaction();
        $success = false;
        try {
            $user = new User(array(
                'first_name' => $data['firstName'],
                'last_name' => $data['lastName'],
                'email' => $data['email'],
                'password' => Hash::make($data['password'])
                    )
            );
            $user->save();
            DB::commit();
            return true;
        } catch (Exception $ex) {
            echo " rollback";
            DB::rollback();
        }
        return false;
    }

    public function update($id, array $input) {
        $oldUser = User::find($id);
        $newUser = $input;
        try {
            $affectedRows = User::where('email', '=', $oldUser['email'])->update(array(
                'first_name' => $newUser['firstName'],
                'last_name' => $newUser['lastName'],
                'email' => $newUser['email'],
            ));
            if ($newUser['newPassword'] != '') {
                $query = User::Where('email', '=', $oldUser['email'])->update(array(
                    'password' => Hash::make($newUser['newPassword'])
                ));
            }
        } catch (Exception $e) {
            return Redirect::route('users.index')->with('message', 'Query failed.');
        }

        //return true;
    }

}
