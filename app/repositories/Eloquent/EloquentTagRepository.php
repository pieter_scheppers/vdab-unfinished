<?php

class EloquentTagRepository extends AbstractRepository {

    protected $modelClassName = 'Tag';

    public function allAmount() {
        $result = DB::table('tags')
                ->select(array('tags.id', 'tags.name', DB::raw('COUNT(task_tags.tag_id) as  amount')))
                ->leftjoin('task_tags', 'tags.id', '=', 'task_tags.tag_id')
                ->groupBy(DB::raw('tags.id'))
                ->orderBy(DB::raw('tags.id', 'desc'))
                ->get();
        return $result;
    }

    public function getRequiredTags($id) {
        $tag = Tag::find($id);
        return $tag->requiredTasks;
    }

    // update a single row
    public function update($id, array $input) {
        $oldTag = Tag::find($id);
        $newTag = $input['modifyName'];
        var_dump($oldTag);
        var_dump($newTag);

        try {
            $affectedRows = Tag::where('name', '=', $oldTag->name)->update(array('name' => $newTag));
        } catch (Exception $e) {
            return Redirect::route('tags.index')->with('message', 'Query failed.');
        }
    }

    public function create(array $data) {
        $tag = new Tag(
                array(
            'name' => $data['tagName']
                )
        );
        $tag->save();
        return true;
        // return $task;
    }

    public function destroy($ids) {
        Tag::destroy($ids);
        return true;
    }

    public function findTagByName($name) {
        return Tag::where('name', $name)->first();
    }

    public function getTop($number) {
        return Tag::take($number)->get();
    }

}
