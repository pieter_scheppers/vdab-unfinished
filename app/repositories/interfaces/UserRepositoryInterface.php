<?php

interface UserRepositoryInterface extends RepositoryInterface {

    public function findUserByEmail($email);
}
