<?php

class Tag extends Eloquent {

    protected $fillable = array('name');
    public static $rules = array(
        'name' => 'required|alpha|min:2'
    );
    protected $table = 'tags';

    public function tasks() {
        return $this->belongsToMany('Task', 'task_tags', 'tag_id', 'task_id');
    }

    protected static function boot() {
        parent::boot();
        static::deleting(function($tag) { // called BEFORE delete()
            $tag->tasks()->detach();
        });
    }

}

?>