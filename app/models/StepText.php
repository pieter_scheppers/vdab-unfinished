<?php

class StepText extends Eloquent {

    protected $fillable = array('task_step_id', 'title', 'text');
    protected $table = 'step_texts';

    public function Step() {
        return $this->belongsTo('TaskStep');
    }

}

?>