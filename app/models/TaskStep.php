<?php

class TaskStep extends Eloquent {

    protected $fillable = array('task_id', 'xp_weight', 'type');
    protected $table = 'task_steps';

    public function Task() {
        return $this->belongsTo('Task');
    }

    public function StepType() {
        return $this->hasOne($this->type);
    }

    protected static function boot() {
        parent::boot();
        static::deleting(function($step) { // called BEFORE delete()
            if ($step->StepText != null)
                $step->StepText->delete();
            if ($step->StepQuestion != null)
                $step->StepQuestion->delete();
        });
    }

}

?>