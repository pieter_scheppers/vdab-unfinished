<?php

class StepQuestion extends Eloquent {

    protected $fillable = array('task_step_id', 'question', 'answer');
    protected $table = 'step_questions';

    public function Step() {
        return $this->belongsTo('TaskStep');
    }
   
}

?>