<?php

class Task extends Eloquent {

    protected $fillable = array('title', 'description', 'xp');
    public static $rules = array(
        'title' => 'required|alpha|min:2',
        'description' => 'required|alpha|min:10',
        'xp' => 'required|num'
    );
    protected $table = 'tasks';

    public function requiredTasks() {
        return $this->belongsToMany('Task', 'tasks_req', 'task_id', 'req_id');
    }

    public function isRequiredBy() {
        return $this->belongsToMany('Task', 'tasks_req', 'req_id', 'task_id');
    }

    public function tags() {
        return $this->belongsToMany('Tag', 'task_tags', 'task_id', 'tag_id');
    }

    public function steps() {
        return $this->hasMany('TaskStep');
    }

    protected static function boot() {
        parent::boot();
        static::deleting(function($task) { // called BEFORE delete()
            $task->isRequiredBy()->detach();
            $task->requiredTasks()->detach();
            $task->tags()->detach();
            foreach ($task->steps as $step) {
                $step->delete(); // Causes any child "deleted" events to be called
            }
        });
    }

}

?>