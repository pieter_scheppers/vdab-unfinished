<?php


use Dingo\Api\Dispatcher;
use Dingo\Api\Auth\Shield;
use Dingo\Api\Http\ResponseBuilder;

class BaseApiController extends BaseController {

    public function __construct(Dispatcher $api, Shield $auth, ResponseBuilder $response)
    {
        parent::__construct($api, $auth, $response);
    }

    public function withError($error, $statusCode)
    {
        if (! is_array($error)) {
            $error = ['message' => $error];
        }

        $error = array_merge(['status'  => $statusCode], $error);

        return $this->setStatusCode($statusCode)->withArray($error);
    }

}
