<?php


class UserApiV2Controller extends \BaseApiController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return $this->withCollection(User::all(), new UserTransformer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $user = User::find($id);
        if( !$user ) {
            return $this->errorNotFound();
        }

        return $this->withItem($user, new UserTransformer);
    }

}
