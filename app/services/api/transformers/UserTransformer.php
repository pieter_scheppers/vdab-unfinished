<?php


use League\Fractal;

class UserTransformer extends Fractal\TransformerAbstract
{

    public function transform(User $user)
    {
        return [
            'id'        => (int) $user->id,
            'name'      => $user->first_name .' '. $user->last_name,
            'email'     => $user->email
        ];
    }

}