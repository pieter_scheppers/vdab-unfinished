<?php

class TaskController extends \BaseController {

    public function __construct(EloquentTaskRepository $taskRepository, EloquentTagRepository $tagRepository) {
        $this->taskRepository = $taskRepository;
        $this->tagRepository = $tagRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $tasks = $this->taskRepository->all();
        return View::make('tasks.index')
                        ->with('tasks', $tasks);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        $tasks = $this->taskRepository->all();
        $tags = $this->tagRepository->getTop(10);
        return View::make('tasks.create2')
                        ->with('tasks', $tasks)
                        ->with('tags', $tags);
    }

    /*
     * show the form for creating new tags
     * @return Response
     */

    public function tags() {
        $tags = $this->taskRepository->all();
        return View::make('tags.tag')
                        ->with('tags', $tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        $input = Input::except('_token');
        $success = $this->taskRepository->create($input);
        if ($success)
            return Redirect::to('/tasks')
                            ->with('message', 'successfully created task')
                            ->with('type', 'success');
        else
            return Redirect::to('/tasks/create')
                            ->with('message', 'could not create task')
                            ->with('type', 'warning');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        $task = $this->taskRepository->find($id);
        $reqs = $this->taskRepository->getRequiredTasks($id);
        $steps = $this->taskRepository->getSteps($id);
        Return View::make('tasks.show')
                        ->with('task', $task)
                        ->with('steps', $steps)
                        ->with('reqs', $reqs);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $task = $this->taskRepository->find($id);
        $tasks = $this->taskRepository->all();
        $tags = $this->tagRepository->getTop(10);
        $taskTags = $task->tags()->lists('name');
        $taskReqs = $task->requiredTasks()->lists('title');
        $steps = $this->taskRepository->getSteps($id);

        Return View::make('tasks.edit')
                        ->with('tasks', $tasks)
                        ->with('taskTags', $taskTags)
                        ->with('tags', $tags)
                        ->with('reqs', $taskReqs)
                        ->with('steps', $steps)
                        ->with('task', $task);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $input = Input::except('_token');
        $success = $this->taskRepository->update($id, $input);
        if ($success)
            return Redirect::to('/tasks')
                            ->with('message', 'successfully updated task')
                            ->with('type', 'success');
        else
            return Redirect::to('/tasks/create')
                            ->with('message', 'could not update task')
                            ->with('type', 'warning');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $response = $this->taskRepository->destroy($id);
        return $response;
    }

}
