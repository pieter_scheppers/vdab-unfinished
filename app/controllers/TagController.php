<?php

class TagController extends \BaseController {

    protected $tagRepository;

    public function __construct(EloquentTagRepository $tagRepository) {
        $this->tagRepository = $tagRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     *      
     */
    public function index() {
        $tags = $this->tagRepository->allAmount();
        return View::make('tags.index')
                        ->with('tags', $tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        $input = Input::except('_token');
        $all = Tag::all();
        foreach ($all as $item) {
            if ($item->name == $input['tagName']) {
                return Redirect::to('/tags')
                                ->with('message', 'Tag already exists')
                                ->with('type', 'warning');
            }
        }
        if ($input['tagName'] != '') {
            $this->tagRepository->create($input);
            return Redirect::to('/tags')
                            ->with('message', 'Tag saved')
                            ->with('type', 'success');
        } else {
            return Redirect::to('/tags')
                            ->with('message', 'Please fill in a tag name before saving')
                            ->with('type', 'warning');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $tag = $this->tagRepository->find($id);
        $input = Input::except('_method', '_token');
        $all = Tag::all();
        if ($input['modifyName'] == '') {
            return Redirect::to('/tags')
                            ->with('message', 'Please fill in a tag name before saving')
                            ->with('type', 'warning');
        } else {
            foreach ($all as $item) {
                if ($item->name == $input['modifyName']) {
                    return Redirect::to('/tags')
                                    ->with('message', 'Tag already exists')
                                    ->with('type', 'warning');
                }
            }
        }

        if ($tag) {
            $this->tagRepository->update($id, $input);
            return Redirect::route('tags.index')
                            ->with('message', 'The tag has been updated!')
                            ->with('type', 'success');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $tag = $this->tagRepository->find($id);
        if ($tag) {
            $this->tagRepository->destroy($id);
        } else {
            Response::make("Page not found", 404);
        }
    }

}
