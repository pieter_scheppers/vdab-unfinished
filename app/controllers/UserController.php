<?php

class UserController extends \BaseController {

    protected $userRepository;

    public function __construct(EloquentUserRepository $userRepository) {
        $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $users = $this->userRepository->all();
        return View::make('users.index')
                        ->with('users', $users);
    }

    public function create() {
        //
    }

    public function store() {
        $input = Input::except('_token');
        if ($input['firstName'] == '' || $input['lastName'] == '' || $input['email'] == '') {
            return Redirect::to('/users')
                            ->with('message', 'Please fill all the required fields in')
                            ->with('type', 'warning');
        }
        elseif ($input['retypePassword'] != $input['password']) {
            return Redirect::to('/users')
                            ->with('message', 'Passwords did not match')
                            ->with('type', 'warning');
        } else {
            $this->userRepository->create($input);
            return Redirect::route('users.index')
                            ->with('message', 'User created')
                            ->with('type', 'success');
        }
    }

    public function show($id) {
        $user = $this->userRepository->find($id);
        Return View::make('users.show')
                        ->with('users', $user);
    }

    public function edit($id) {
        return View::make('users.index')
                        ->with('page', 'users.edit');
    }

    public function update($id) {
        $user = $this->userRepository->find($id);
        $input = Input::except('_method', '_token');
        if ($input['newPassword'] != $input['retypePassword']) {
            return Redirect::to('/users')
                            ->with('message', 'Passwords did not match')
                            ->with('type', 'warning');
        } elseif ($user) {
            $this->userRepository->update($id, $input);
            return Redirect::route('users.index')
                            ->with('message', 'The user has been updated!')
                            ->with('type', 'success');
        }
    }

    public function destroy($id) {
        $user = $this->userRepository->find($id);
        if ($user) {
            $this->userRepository->destroy($id);
        } else {
            Response::make("Page not found", 404);
        }
    }

}
