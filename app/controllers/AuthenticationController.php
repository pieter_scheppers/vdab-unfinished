<?php

class AuthenticationController extends \BaseController {

    protected $userRepository;

    public function __construct(EloquentUserRepository $userRepository) {
        $this->userRepository = $userRepository;
    }

    public function showLogin() {
        return View::make('authentication.login');
    }

    public function processLogin() {
        $input = Input::all();

        $email = HTML::entities($input['email']);
        $password = $input['password'];
        // $userRepository = App::make('EloquentUserRepository');
        // $user = $userRepository->findByEmail($email);

        $user = $this->userRepository->findUserByEmail($email);

        if (!$user) {
            return Redirect::to('/login')->with('message', 'Email address not found')
                            ->with('type', 'warning');
        }
        $credentials = array(
            'email' => $email,
            'password' => $password
        );
        if (Auth::attempt($credentials)) {
            return Redirect::to('/')->with('message', 'Logged in')
                            ->with('type', 'success')
                            ->with('user', $email);
        }
        return Redirect::to('/login')->with('message', 'Login failed')
                        ->with('type', 'danger');
    }

    public function logout() {

        if (Auth::check()) {
            Auth::logout();
        } else {
            Redirect::to('login');
        }
        return Redirect::to('login')->with('message', 'Logged out')
                        ->with('type', 'success');
    }

}
