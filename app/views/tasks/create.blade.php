@extends('layouts.dashboard')
@section('content')
<div data-alerts="alerts" data-ids="myid" data-fade="4000"></div>

<h1 class="page-header">Add new task</h1>

<ul class="nav nav-tabs">
    <li class="active"><a href="#tab1" data-toggle="tab">Basic info</a></li>
    <li><a href="#tab2" data-toggle="tab">Task Choice</a></li>
    <li><a href="#tab3" data-toggle="tab">Summary</a></li>
</ul><!-- /nav tabs -->
<div class="tab-content">
    <div class="tab-pane active" id="tab1">           
        <br/>
        {{ Form::open(array('class'=>'form-horizontal','files'=> true, 'id'=>'newTask')) }}
        <fieldset>
            <!-- title -->
            <div class="form-group">
                {{ Form::label('title', 'Title', array('class' => 'col-xs-2 control-label')) }}
                <div class="col-xs-5 col-md-3">
                    {{ Form::text('title', null, array('class'=>'form-control input-md','required')) }}
                </div>
            </div>			
            <!-- xp -->
            <div class="form-group">
                {{ Form::label('xp', 'Amount of XP', array('class' => 'col-xs-2 col-md-2 control-label')) }}
                <div class="col-xs-3 col-md-2">
                    {{ Form::input('number', 'xp', null, array('class'=>'form-control input-md','required')) }}
                </div>
            </div>
            <!-- description -->
            <div class="form-group">
                {{ Form::label('description', 'Description', array('class' => 'col-xs-2 control-label')) }}			
                <div class="col-xs-4 col-md-4">						
                    {{ Form::textarea('description', null, array('class'=>'form-control','required')) }}
                </div>
            </div>
            <!-- required task steps -->
            <div class="form-group">

                {{ Form::label('required', 'Required tasks', array('class' => 'col-xs-2 col-md-2 control-label')) }}
                <div id="reqsList" class="col-xs-5 col-md-4">
                    <i>no required tasks selected</i>
                </div>
                <div class="col-xs-3 col-md-3">
                    {{ Form::button('Choose tasks', array( 'class'=>'btn btn-md btn-default', 'data-toggle'=>'modal', 'data-target'=>'#reqModal')) }}
                </div>

            </div>
            <!-- add tags -->
            <div class="form-group">
                {{ Form::label('tags', 'Tags', array('class' => 'col-xs-2 col-md-2 control-label')) }}			
                <div class="col-xs-2 col-md-2">	
                    {{ Form::text('tags', null, array('class'=>'form-control input-md','id'=>'input_tag','required')) }}
                </div>
                <div class="col-xs-6 col-md-6">
                    {{ Form::button('Add', array('class'=>'btn btn-default', 'id'=>'add_tag')) }}
                </div>                
                <div class="col-xs-4 col-xs-offset-2" id="tag_list">
                    <i>seperate tags with commas</i>
                    <div id="selected_tags"></div>
                    <div class="panel-group" id="tags_accordion">
                        <div class="panel">
                            <a data-toggle="collapse" data-parent="#accordion" href="#tags_collapse">
                                Choose from the most used tags
                            </a>
                            <div id="tags_collapse" class="panel-collapse collapse">
                                <div class="panel-body">
                                    secetariaat, jobs, student, more tags can be loaded once we have a table
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{ Form::button('Save Task', array('class'=>'btn btn-success','id'=>'copy')) }}
        </fieldset>
        {{ Form::close() }}
        <br>
        <a class="btn btn-primary btnNext">Next</a>
    </div> <!-- /tab1 -->

    <div class="tab-pane" id="tab2">
        <br>
        <div class="btn-group">
            <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                Add Task Step <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
                <li><a href="#" onClick="(createStep('read'))">Read Text</a></li>
                <li><a href="#" onClick="(createStep('open'))">Open Question</a></li>
                <li><a href="#" onClick="(createStep('yesno'))">Yes/no Question</a></li>
                <li><a href="#" onClick="(createStep('choice'))">Multiple Choice Question</a></li>
                <li><a href="#" onClick="(createStep('rating'))">Give Rating</a></li>
                <li><a href="#" onClick="(createStep('reminder'))">Reminder</a></li>
            </ul>
        </div>
        <br>
        <br>
        <div class="panel-group" id="accordion">

        </div><!-- /accordion -->

        <br/>
        <a class="btn btn-primary btnPrevious">Previous</a>
        <a class="btn btn-primary btnNext">Next</a>
        <a class="btn btn-success" id="btnCopyStepsForm">Save</a>
    </div><!-- /tab2 -->

    <div class="tab-pane" id="tab3">
        <br/>
        <div class="panel panel-primary">
            <div class="panel-heading">Your task</div>
            {{ Form::open(array('url'=>'tasks', 'method'=>'post', 'class'=>'form-horizontal', 'id'=>'createTask','name'=>'createTask')) }}
            <fieldset>
                <ul class="list-group">     

                    <li class="list-group-item" name="title">
                        <div class="row">
                            <label class='col-xs-2 control-label'>Title: </label>
                            <div class="col-xs-4">
                                {{ Form::text('title',  null, array('class'=>'form-control input-md', 'disabled')) }}
                            </div>
                        </div>
                    </li>

                    <li class="list-group-item" name="xp">
                        <div class="row">
                            <label class='col-xs-2 control-label'>XP:</label>	
                            <div class="col-xs-4">
                                {{ Form::input('number', 'xp', null, array('class'=>'form-control input-md','disabled')) }}
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item" name="description">
                        <div class="row">
                            <label class='col-xs-2 control-label'>Description: </label>
                            <div class="col-xs-4">
                                {{ Form::textarea('description',  null, array('class'=>'form-control input-md','disabled')) }}
                            </div>
                        </div>                        
                    </li>
                    <li class="list-group-item" name="required">
                        <div class="row">
                            {{ Form::label('required', 'required', array('class' => 'col-xs-2 control-label')) }}  
                            <div class="col-xs-4">
                                {{ Form::textarea('required_box',  null, array('class'=>'form-control input-md required_box','rows'=>'4', 'disabled', 'id'=>'required_box')) }}
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item" name="tag">
                        <div class="row">
                            <label class='col-xs-2 control-label'>Title: </label>
                            <div class="col-xs-4">
                                {{ Form::text('tag',  null, array('class'=>'form-control input-md', 'disabled')) }}
                            </div>
                            <div id="hidden_tagfield"></div>
                        </div>
                    </li>
                    <li class="list-group-item" name="task_choices">
                        <div  id="summary-form" >
                        </div><!-- /copied steps from tab 2 -->    
                    </li>
                    {{ Form::submit('Save Task', array('class'=>'btn btn-success')) }}
                </ul><!-- /summary form -->
            </fieldset>
            {{ Form::close() }}

        </div>


        <br/>
        <a class="btn btn-primary btnPrevious">Previous</a>
    </div><!-- /tab3 -->

</div><!-- /tab content -->

<!-- Modals -->
<div class="modal fade" id="reqModal" tabindex="-1" role="dialog" aria-labelledby="searchTaskModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Search for required tasks</h4>
            </div>
            <div class="modal-body">                
                {{ Form::open(array('url' => 'foo/bar')) }}
                @foreach($tasks as $task)
                <div class="col-sm-4">
                    <div class="input-group">
                        {{ Form::checkbox('reqTask', $task->title, false, array('id'=>$task->id, 'class' => 'reqTask'))}} {{$task->title}}
                    </div><!-- /input-group -->
                </div><!-- /.col-sm-4 -->

                @endforeach       
                {{ Form::close() }}
                <br/><br/><br/><br/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onClick="addTasks()" data-dismiss="modal">Submit</button>
            </div>
        </div>
    </div>
</div><!-- /modal -->
<div class="hidden">
    <div id="reading-dropdown">
        <div class="panel panel-default" id="default-step">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse">Reading task</a>
                    <a class="btn btn-xs btn-danger pull-right" onClick="deleteStep(this)"><i class="fa fa-minus-circle fa-sm fa-inverse"> Remove</i></a>
                    <div class="clearfix"></div>
                </h4>
            </div>
            <div id="collapse" class="panel-collapse collapse">
                <div class="panel-body">
                    {{ Form::open(array('class'=>'form-horizontal', 'id'=>'task_read')) }}
                    <fieldset>
                        <!-- step weight -->
                        <div class="form-group">
                            {{ Form::label('step_weight', 'weight', array('class' => 'col-md-2 control-label')) }}
                            <div class="col-md-4">
                                {{ Form::select('step_weight', array(1=>1,2=>2,3=>3,4=>4,5=>5), null, array('id'=>'step_weight','class' => 'form-control')) }}
                            </div>
                        </div>	
                        <!-- choice_title -->
                        <div class="form-group">
                            {{ Form::label('read_title', 'Title', array('class' => 'col-md-2 control-label')) }}
                            <div class="col-md-4">
                                {{ Form::text('read_title',  null, array('class'=>'form-control input-md ')) }}
                            </div>
                        </div>			
                        <!-- text_to_read -->
                        <div class="form-group">
                            {{ Form::label('text_to_read', 'Text to read', array('class' => 'col-md-2 control-label')) }}	                              
                            <div class="col-md-4">						
                                {{ Form::textarea('text_to_read', null, array('class'=>'form-control','placeholder'=>'Fill in the text you want them to read and confirm')) }}
                            </div>
                        </div>                            
                        <br/>
                        {{ Form::hidden('choice','read') }}
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div><!-- reading-dropdown -->

    <div id="open-dropdown">  
        <div class="panel panel-default" id="default-step">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse">
                        Question/quizz task
                    </a>
                    <a class="btn btn-xs btn-danger pull-right" onClick="deleteStep(this)"><i class="fa fa-minus-circle fa-sm fa-inverse"> Remove</i></a>
                    <div class="clearfix"></div>                </h4>
            </div>
            <div id="collapse" class="panel-collapse collapse">
                <div class="panel-body">
                    {{ Form::open(array('class'=>'form-horizontal', 'id'=>'task_question')) }}
                    <fieldset>
                        <!-- step weight -->
                        <div class="form-group">
                            {{ Form::label('step_weight', 'weight', array('class' => 'col-xs-2 col-sm-2 control-label')) }}
                            <div class="col-md-4">
                                {{ Form::select('step_weight', array(1=>1,2=>2,3=>3,4=>4,5=>5), null, array('id'=>'step_weight','class' => 'form-control')) }}
                            </div>
                        </div>	
                        <!-- choice_title -->
                        <div class="form-group">
                            {{ Form::label('question_title', 'Question', array('class' => 'col-xs-2 col-sm-2 control-label')) }}
                            <div class="col-md-4">
                                {{ Form::text('question_title',null, array('class'=>'form-control input-md ')) }}
                            </div>
                        </div>			
                        <!-- text_to_read -->
                        <div class="form-group">
                            {{ Form::label('question_answer', 'Answer', array('class' => 'col-xs-2 col-sm-2 control-label')) }}	                              
                            <div class="col-md-4">						
                                {{ Form::textarea('question_answer', null, array('class'=>'form-control','rows'=>'4','placeholder'=>'Write here the possible answer(s) to your question. (Seperated by a new line)')) }}
                            </div>
                        </div>                            
                        <br/>
                        {{ Form::hidden('choice','question') }}
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    <!-- TODO: Create more step types -->
    <div id="yesno-dropdown"></div>
    <div id="multiple-dropdown"></div>
    <div id="rating-dropdown"></div>
    <div id="reminder-dropdown"></div>

</div><!-- /Hidden HTML -->


@stop