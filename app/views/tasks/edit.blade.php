@extends('layouts.dashboard')
@section('content')
<div data-alerts="alerts" data-ids="myid" data-fade="4000"></div>

<div class="row form-group">
    <div class="col-xs-12">
        <ul class="nav nav-pills nav-justified setup-panel">
            <li class="active"><a href="#panel-1">
                    <h4 class="list-group-item-heading">Basic Info</h4>
                    <p class="list-group-item-text">Edit basic info</p>
                </a></li>
            <li class=""><a href="#panel-2">
                    <h4 class="list-group-item-heading">Steps</h4>
                    <p class="list-group-item-text">Edit required steps for task</p>
                </a></li>
            <li class=""><a href="#panel-3">
                    <h4 class="list-group-item-heading">Summary</h4>
                    <p class="list-group-item-text">Update task</p>
                </a></li>
        </ul>
    </div>
</div>
<div class="row setup-content hidden" id="panel-1">
    <div class="col-xs-12">
        <div class="col-md-12 well text-left">
            {{ Form::open(array('class'=>'form-horizontal','files'=> true, 'id'=>'task-info')) }}
            <fieldset>
                <!-- title -->
                <div class="form-group">
                    {{ Form::label('title', 'Title', array('class' => 'col-md-2 control-label')) }}
                    <div class="col-md-4">
                        {{ Form::text('title', $task['title'], array( 'data-bv-notempty'=>'true', 'data-bv-notempty-message'=>'The first name is required and cannot be empty','class'=>'form-control input-md','required')) }}
                    </div>
                </div>			
                <!-- xp -->
                <div class="form-group">
                    {{ Form::label('xp', 'Amount of XP', array('class' => 'col-md-2 control-label')) }}
                    <div class="col-md-2">
                        {{ Form::input('number', 'xp', $task['xp'], array('min'=>0, 'class'=>'form-control input-md','required')) }}
                    </div>
                </div>
                <!-- description -->
                <div class="form-group">
                    {{ Form::label('description', 'Description', array('class' => 'col-md-2 control-label')) }}			
                    <div class="col-md-4">						
                        {{ Form::textarea('description', $task['description'], array('class'=>'form-control','required')) }}
                    </div>
                </div>
                <!-- required task steps -->
                <div class="form-group">

                    {{ Form::label('required', 'Required tasks', array('class' => 'col-md-2 control-label')) }}
                    <div id="reqsList" class="col-md-4">
                        <i>no required tasks selected</i>
                    </div>
                    <div class="col-xs-3 col-md-3">
                        {{ Form::button('Choose tasks', array( 'class'=>'btn btn-md btn-default', 'data-toggle'=>'modal', 'data-target'=>'#reqModal')) }}
                    </div>

                </div>
                <!-- add tags -->
                <div class="form-group">
                    {{ Form::label('tags', 'Tags', array('class' => 'col-md-2 control-label')) }}
                    <div class="col-md-4">	
                        {{ Form::text('tags', null, array('data-role'=>'tagsinput', 'class'=>'form-control input-md required','id'=>'input-tag')) }}
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-4 col-md-offset-2" id="tag_list">
                            <div id="selected_tags"></div>
                            <div class="panel-group" id="tags_accordion">
                                <div> Type tags and press enter or 
                                    <a data-toggle="collapse" data-parent="#accordion" href="#tags_collapse">
                                        Choose from the most used tags
                                    </a>
                                    <div id="tags_collapse" class="panel panel-collapse collapse">
                                        <div class="panel-body">
                                            @foreach($tags as $tag)
                                            <a style="cursor:pointer;" class="add-tag">{{$tag->name}}</a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::button('Save', array('class'=>'btn btn-primary','id'=>'activate-panel-2')) }}
            </fieldset>
            {{ Form::close() }}
        </div>
    </div>
</div> <!-- step 1 -->
<div class="row setup-content hidden" id="panel-2">
    <div class="col-xs-12">
        <div class="col-md-12 well">
            <div class="btn-group">
                <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                    Add Task Step <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#" onClick="(createStep('read'))">Read Text</a></li>
                    <li><a href="#" onClick="(createStep('open'))">Open Question</a></li>
                    <li><a href="#" onClick="(createStep('yesno'))">Yes/no Question</a></li>
                    <li><a href="#" onClick="(createStep('choice'))">Multiple Choice Question</a></li>
                    <li><a href="#" onClick="(createStep('rating'))">Give Rating</a></li>
                    <li><a href="#" onClick="(createStep('reminder'))">Reminder</a></li>
                </ul>
            </div>
            <br>
            <br>
            <div class="panel-group" id="accordion">

            </div><!-- /accordion -->

            <br/>
            <a class="btn btn-primary btnPrevious">Previous</a>
            <a class="btn btn-primary" id="activate-panel-3">Save</a>
        </div>
    </div>
</div>
<div class="row setup-content hidden" id="panel-3">
    <div class="col-xs-12">
        <div class="col-md-12 well">
            <div id="summary">
                <table class="table table-bordered table-striped">
                    <colgroup>
                        <col class="col-xs-1">
                        <col class="col-xs-7">
                    </colgroup>
                    <thead>
                        <tr>
                            <th colspan="2" class="success">Basic info</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>
                                Title
                            </th>
                            <td id="title"></td>
                        </tr>
                        <tr>
                            <th>
                                XP
                            </th>
                            <td id="xp"></td>
                        </tr>
                        <tr>
                            <th>
                                Description
                            </th>
                            <td id="description"></td>
                        </tr>
                        <tr>
                            <th>
                                Tags
                            </th>
                            <td id="tags"></td>
                        </tr>
                        <tr>
                            <th>
                                Required Tasks
                            </th>
                            <td id="tasks"></td>
                        </tr>
                    </tbody>
                </table>

                <table class="table table-bordered table-striped">
                    <colgroup>
                        <col class="col-xs-1">
                        <col class="col-xs-7">
                    </colgroup>
                    <thead>
                        <tr>
                            <th colspan="2" class="success">Task Steps</th>
                        </tr>
                    </thead>
                    <tbody id="summarySteps">

                    </tbody>
                </table>
            </div>
            {{ Form::open(array('url'=>'tasks/'.$task->id, 'method'=>'put','class'=>'form-horizontal','files'=> true, 'id'=>'summaryForm')) }}
            <div id="summaryFormBasic"></div>
            <div id="summaryFormSteps"></div>
            <a class="btn btn-primary btnPrevious">Previous</a>
            {{ Form::submit('Update Task', array( 'class'=>'btn btn-md btn-success')) }}
            {{ Form::close() }}
        </div>
    </div>
</div>
<!-- Modals -->
<div class="modal fade" id="reqModal" tabindex="-1" role="dialog" aria-labelledby="searchTaskModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Search for required tasks</h4>
            </div>
            <div class="modal-body">                
                {{ Form::open(array('url' => 'foo/bar')) }}
                @foreach($tasks as $task)
                <div class="col-sm-4">
                    <div class="input-group">
                        {{ Form::checkbox('reqTask', $task->title, false, array('id'=>$task->id, 'class' => 'reqTask'))}} {{$task->title}}
                    </div><!-- /input-group -->
                </div><!-- /.col-sm-4 -->

                @endforeach       
                {{ Form::close() }}
                <br/><br/><br/><br/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onClick="addTasks()" data-dismiss="modal">Submit</button>
            </div>
        </div>
    </div>
</div><!-- /modal -->
<div class="hidden">
    <div id="reading-dropdown">
        <div class="panel panel-default" id="default-step">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse">Reading task</a>
                    <a class="btn btn-xs btn-danger pull-right" onClick="deleteStep(this)"><i class="fa fa-minus-circle fa-sm fa-inverse"> Remove</i></a>
                    <div class="clearfix"></div>
                </h4>
            </div>
            <div id="collapse" class="panel-collapse collapse">
                <div class="panel-body">
                    {{ Form::open(array('class'=>'form-horizontal', 'id'=>'task_read')) }}
                    <fieldset>
                        <!-- step weight -->
                        <div class="form-group">
                            {{ Form::label('step_weight', 'weight', array('class' => 'col-md-2 control-label')) }}
                            <div class="col-md-4">
                                {{ Form::select('step_weight', array(1=>1,2=>2,3=>3,4=>4,5=>5), null, array('id'=>'step_weight','class' => 'form-control')) }}
                            </div>
                        </div>	
                        <!-- choice_title -->
                        <div class="form-group">
                            {{ Form::label('read_title', 'Title', array('class' => 'col-md-2 control-label')) }}
                            <div class="col-md-4">
                                {{ Form::text('read_title',  null, array('class'=>'form-control input-md ')) }}
                            </div>
                        </div>			
                        <!-- text_to_read -->
                        <div class="form-group">
                            {{ Form::label('text_to_read', 'Text to read', array('class' => 'col-md-2 control-label')) }}	                              
                            <div class="col-md-4">						
                                {{ Form::textarea('text_to_read', null, array('class'=>'form-control','placeholder'=>'Fill in the text you want them to read and confirm')) }}
                            </div>
                        </div>                            
                        <br/>
                        {{ Form::hidden('choice','read') }}
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div><!-- reading-dropdown -->

    <div id="open-dropdown">  
        <div class="panel panel-default" id="default-step">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse">
                        Question/quizz task
                    </a>
                    <a class="btn btn-xs btn-danger pull-right" onClick="deleteStep(this)"><i class="fa fa-minus-circle fa-sm fa-inverse"> Remove</i></a>
                    <div class="clearfix"></div>                </h4>
            </div>
            <div id="collapse" class="panel-collapse collapse">
                <div class="panel-body">
                    {{ Form::open(array('class'=>'form-horizontal', 'id'=>'task_question')) }}
                    <fieldset>
                        <!-- step weight -->
                        <div class="form-group">
                            {{ Form::label('step_weight', 'weight', array('class' => 'col-xs-2 col-sm-2 control-label')) }}
                            <div class="col-md-4">
                                {{ Form::select('step_weight', array(1=>1,2=>2,3=>3,4=>4,5=>5), null, array('id'=>'step_weight','class' => 'form-control')) }}
                            </div>
                        </div>	
                        <!-- choice_title -->
                        <div class="form-group">
                            {{ Form::label('question_title', 'Question', array('class' => 'col-xs-2 col-sm-2 control-label')) }}
                            <div class="col-md-4">
                                {{ Form::text('question_title',null, array('class'=>'form-control input-md ')) }}
                            </div>
                        </div>			
                        <!-- text_to_read -->
                        <div class="form-group">
                            {{ Form::label('question_answer', 'Answer', array('class' => 'col-xs-2 col-sm-2 control-label')) }}	                              
                            <div class="col-md-4">						
                                {{ Form::textarea('question_answer', null, array('class'=>'form-control','rows'=>'4','placeholder'=>'Write here the possible answer(s) to your question. (Seperated by a new line)')) }}
                            </div>
                        </div>                            
                        <br/>
                        {{ Form::hidden('choice','question') }}
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    <!-- TODO: Create more step types -->
    <div id="yesno-dropdown"></div>
    <div id="multiple-dropdown"></div>
    <div id="rating-dropdown"></div>
    <div id="reminder-dropdown"></div>

</div><!-- /Hidden HTML -->
@stop
@section('scripts')
@parent
<script>
    $(document).ready(function() {
//Setup
        var navListItems = $('ul.setup-panel li a'),
                allWells = $('.setup-content');
        allWells.hide();
        navListItems.click(function(e)
        {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                    $item = $(this).closest('li');
            if (!$item.hasClass('disabled')) {
                navListItems.closest('li').removeClass('active');
                $item.addClass('active');
                allWells.hide();
                $target.show();
            }
            allWells.removeClass('hidden');
        });
        $('ul.setup-panel li.active a').trigger('click');
        $('.btnPrevious').on('click', function(e) {
            $('ul.setup-panel li.active').prev().find('a').trigger('click');
        });
//Basic info

        // Choose tag from most used tags //
        $('.add-tag').on('click', function() {
            var text = $(this).text();
            $('#input-tag').tagsinput('add', text);
        });
        // Save step 1 info and go to second step //
        $('#activate-panel-2').on('click', function(e) {
            if ($('#xp').val() == '' || $('#description').val() == '' || $('#title').val() == '' || $('#input-tag').val() == '') {
                warningReqFields();
            } else {
                saveInfo();
                $('ul.setup-panel li:eq(1)').removeClass('disabled');
                $('ul.setup-panel li a[href="#panel-2"]').trigger('click');
                $(this).text('save');
            }
        });
//---Step 2---//

        // Save step 2 info and go to third step //
        $('#activate-panel-3').on('click', function(e) {
            var areStepsFilledIn = true;
            $('#accordion').find(':input').each(function() {
                if (this.value == '')
                    areStepsFilledIn = false;
            });
            if ($("#accordion").children().length == 0 || !areStepsFilledIn) {
                warningReqFields();
            }
            else {
                saveSteps();
                $('ul.setup-panel li:eq(2)').removeClass('disabled');
                $('ul.setup-panel li a[href="#panel-3"]').trigger('click');
                $(this).text('save');
            }
        });

//Alerts
        function warningReqFields() {
            window.scrollTo(0, 0);
            $(document).trigger("add-alerts", [{
                    'message': "Please fill in all the required fields before saving",
                    'priority': 'warning'
                }
            ]);
        }

    });
    // Copy selected required tasks
    function addTasks() {
        var reqsList = document.getElementById('reqsList');
        var requiredTasks = $('.reqTask:checked').map(function() {
            return {id: this.id, title: this.value};
        }).get();
        //reqsList.innerHTML = (requiredTasks.lengh > 0) ? '' : '<i>no required tasks</i>';
        if (requiredTasks.length > 0) {
            reqsList.innerHTML = '';
        }
        else {
            reqsList.innerHTML = '<i>no required tasks</i>';
        }
        requiredTasks.forEach(function(task) {

            reqsList.innerHTML += '<a  class="list-group-item-custom removeRequired' + task['id'] + '" onClick="removeRequired(' + task['id'] + ')"><i class="fa fa-times-circle"></i>&nbsp;' + task['title'] + '</a>';
        });
    }

    /*
     * remove added tasks on click
     * @param {type} $number
     */
    function removeRequired($number) {
        $('.reqTask#' + $number).attr('checked', false);
        $('.removeRequired' + $number).remove();
        addTasks();
    }

    // create steps
    var stepCounter = 0;
    function createStep(type) {
        console.log('createstep: ' + stepCounter);
        var addedDropdown = false;
        switch (type) {
            case 'read':
                var dropdown = $('#reading-dropdown div').first().clone();
                dropdown.appendTo('#accordion');
                addedDropdown = true;
                break;
            case 'open':
                var dropdown = $('#open-dropdown div').first().clone();
                dropdown.appendTo('#accordion');
                addedDropdown = true;
                break;
        }
        if (addedDropdown) {
            $('#default-step').attr('id', 'step-' + stepCounter);
            $('#step-' + stepCounter + ' div h4 a').attr('href', '#collapse-step-' + stepCounter);
            $('#step-' + stepCounter + ' #collapse').attr('id', 'collapse-step-' + stepCounter);
            $('#accordion div .in').collapse('toggle');
            $("#collapse-step-" + stepCounter).collapse('toggle');
            $('#step-' + stepCounter).find(':input').each(function() {
                var name = $(this).attr('name');
                $(this).attr('name', 'step[' + stepCounter + '][' + name + ']');
            });
            stepCounter++;
        }
    }

//deletestep
    function deleteStep(n) {
        n.parentNode.parentNode.parentNode.remove();
        //stepCounter--;
    }

    function saveSteps() {
        console.log('saving step');
        $('#summarySteps').empty();
        $('#summaryFormSteps').empty();
        var i = 1;
        $("#accordion").children().each(function() {
            var inputValues = $(this).find('fieldset :input').map(function() {
                return {key: this.name, value: this.value};
            }).get();
            $('#summarySteps').append('<tr><th colspan="2" class="info">Step ' + i + ' </th></tr>');
            inputValues.forEach(function(step) {
                $('#summaryFormSteps').append('<input name="' + step['key'] + '" type="hidden" value="' + step['value'] + '">');
                var label = step['key'].split(/[^a-zA-Z]/).slice(1, -1);
                label = label.join(' ');
                console.log('key: ' + step['key'] + ' label: ' + label);
                $('#summarySteps').append('<tr><th>' + label + '</th><td>' + step['value'] + '</td></tr>');
            });
            console.log(inputValues);
            i++;
        });
    }

    function saveInfo() {
        $('#task-info').find(':input[name]').each(function() {
            $('#summary').find('#' + this.name).text(this.value);
            $('#summary').find(':input[name=' + this.name + ']').val(this.value);
            $('#summaryFormBasic').append('<input name="' + this.name + '" type="hidden" value="' + this.value + '">');
            console.log(this.value);
        });
        var reqTasks = $('.reqTask:checked').map(function() {
            console.log('checked task:' + this.value);
            return {id: this.id, title: this.value};
        }).get();
        $('#summary').find('#tasks').text((reqTasks.length > 0) ? '' : 'no required tasks');
        reqTasks.forEach(function(task) {
            $('#summary').find('#tasks').append(task['title'] + ', ');
            $('#summaryFormBasic').append('<input name="tasks[]" type="hidden" value="' + task['title'] + '">');
            //document.createTask.requiredBox.value += task['title'] + '\r\n';
        });
    }
</script>


@foreach ($reqs as $reqTask)
<script>
    $(document).ready(function() {
        var reqName = '{{$reqTask}}';
        console.log('req: ' + reqName);
        $('.reqTask:checkbox').each(function() {
            if (this.value == reqName)
                $(this).attr('checked', true);
            console.log(this.checked);
        });
    });</script>
@endforeach

@foreach ($taskTags as $tag)
<script>
    $(document).ready(function() {
        var tagName = '{{$tag}}';
        console.log('tag: ' + tagName);
        $('#input-tag').tagsinput('add', tagName);
    });</script>
@endforeach

@foreach ($steps as $step)
<script type="text/javascript">
    $(document).ready(function() {
        console.log('{{$step->stepType}}');

        var step = jQuery.parseJSON('{{ $step }}');
        var stepData = jQuery.parseJSON('{{ $step->StepType }}');
        var type = step['type'];

        switch (type) {
            case 'StepText':
                createStep('read');
                $('#step-' + (stepCounter - 1)).find('#step_weight').val(step['xp_weight']);
                $('#step-' + (stepCounter - 1)).find('#read_title').val(stepData['title']);
                $('#step-' + (stepCounter - 1)).find('#text_to_read').val(stepData['text']);
                break;
            case 'StepQuestion':
                createStep('open');
                $('#step-' + (stepCounter - 1)).find('#step_weight').val(step['xp_weight']);
                $('#step-' + (stepCounter - 1)).find('#question_title').val(stepData['question']);
                $('#step-' + (stepCounter - 1)).find('#question_answer').val(stepData['answer']);

                break;
        }
    });
</script>
@endforeach

<script>
    $(document).ready(function() {
        addTasks();
        saveInfo();
        saveSteps();
    });
</script>
@stop