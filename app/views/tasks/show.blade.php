@extends('layouts.dashboard')
@section('content')
<h1 class="page-header">Task Details</h1>

<div class="col-md-12 well">
    <div id="summary">
        <table class="table table-bordered table-striped">
            <colgroup>
                <col class="col-xs-1">
                <col class="col-xs-7">
            </colgroup>
            <thead>
                <tr>
                    <th colspan="2" class="success">Basic info</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>
                        Title
                    </th>
                    <td id="title">{{$task->title}}</td>
                </tr>
                <tr>
                    <th>
                        XP
                    </th>
                    <td id="xp">{{$task->xp}}</td>
                </tr>
                <tr>
                    <th>
                        Description
                    </th>
                    <td id="description">{{$task->description}}</td>
                </tr>
                <tr>
                    <th>
                        Tags
                    </th>
                    <td id="tags">
                        @foreach($task->tags->lists('name') as $tag)
                        {{$tag}}, 
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th>
                        Required Tasks
                    </th>
                    <td id="tasks">
                        @if(count($reqs)>0)

                        @foreach($reqs as $req)
                        {{$req->title}}, 
                        @endforeach
                        @else
                        <i>no tasks</i>
                        @endif
                    </td>
                </tr>
            </tbody>
        </table>

        <table class="table table-bordered table-striped">
            <colgroup>
                <col class="col-xs-1">
                <col class="col-xs-7">
            </colgroup>
            <thead>
                <tr>
                    <th colspan="2" class="success">Task Steps</th>
                </tr>
            </thead>
            <tbody id="summarySteps">
                @for($i = 0; $i < count($steps); $i++)
                <tr><th colspan="2" class="info">Step {{$i+1}} </th></tr>
                <tr><th>XP Weight</th><td>{{$steps[$i]->xp_weight}}</td></tr>
                @if($steps[$i]->type == 'StepText')
                <tr><th>Title</th><td>{{$steps[$i]->stepType->title}}</td></tr>
                <tr><th>Text</th><td>{{$steps[$i]->stepType->text}}</td></tr>
                @elseif($steps[$i]->type == 'StepQuestion')
                <tr><th>Question</th><td>{{$steps[$i]->stepType->question}}</td></tr>
                <tr><th>Answer</th><td>{{$steps[$i]->stepType->answer}}</td></tr>
                @endif
                @endfor
            </tbody>
        </table>
    </div>
</div>

</table>

@stop
