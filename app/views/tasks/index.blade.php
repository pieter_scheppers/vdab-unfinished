@extends('layouts.dashboard')
@section('content')
<h1 class="page-header">Task overview</h1>

<div class="panel panel-default">
    <div class="panel-heading">Manage Tasks</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12"> 
                <table id="table_tasks" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>title</th>
                            <th>Description</th>
                            <th>XP</th>
                            <th>Tags</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>title</th>
                            <th>Description</th>
                            <th>XP</th>
                            <th>Tags</th>
                            <th>Action</th>

                        </tr>
                    </tfoot>

                    <tbody>
                        @foreach($tasks as $task)
                        <tr>
                            <td>{{$task->title}}</td>
                            <td>{{$task->description}}</td>
                            <td>{{$task->xp}}</td>
                            <td>
                                @foreach($task->tags()->lists('name') as $tag)
                                {{$tag}},
                                @endforeach

                            </td>
                            <td class="text-center button"><div class="btn-group">
                                    <a href="{{ URL::to('/tasks/'.$task->id) }}" class="btn btn-xs btn-info">details</a>
                                    <a href="{{ URL::to('/tasks/'.$task->id.'/edit') }}" class="btn btn-xs btn-primary">modify</a>
                                    <a href="javascript:deleteTask({{$task->id}});" class="btn btn-xs btn-danger">delete</a></div></td>
                        </tr>
                        @endforeach       
                    </tbody>
                </table>
                <a href="{{ URL::route('tasks.create') }}" class="btn btn-primary"> Create task </a>
            </div>
        </div>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(document).ready(function() {
        $('#table_tasks').dataTable();
    });
</script>
@stop
@stop
