@extends('layouts.dashboard')
@section('content')
<div data-alerts="alerts" data-ids="myid" data-fade="4000"></div>

<h1 class="page-header">Tag overview</h1>

<div class="panel panel-default">
    <div class="panel-heading">Manage Tags</div>
    <div class="panel-body">
        <div class="row">

            <div class="col-xs-4 col-sm-4 col-md-4">
                <br/>
                {{ Form::open(array('url'=>'/tags', 'role'=>'form', 'class'=>'form-horizontal', 'id'=>'createTag')) }}
                <fieldset>
                    <div class="form-group">
                        {{ Form::label('tagName', 'Name', array('class' => 'col-xs-2 control-label')) }}

                        <div class="col-xs-7">
                            {{ Form::text('tagName',null, array('class'=>'form-control input-md')) }}
                            <span class="help-block">The name is how it appears.</span>
                        </div>
                    </div>
                    {{ Form::submit('Save tag', array('class'=>'btn btn-success','id'=>'saveTag')) }}
                </fieldset>
                {{ Form::close() }}
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8"> <br />
                <table id="tableTags" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Times used</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Times used</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>       
                        @foreach($tags as $tag)
                        <tr>
                            <td>{{$tag->name}}</td>
                            <td>{{$tag->amount}}</td>
                            <td class="text-center button">
                                <div class="btn-group">
                                    <button class="btn btn-xs btn-primary tagModify" data-toggle="modal" id="{{ $tag->name }}" name="modify" data-target=".bs-example-modal-sm">modify</button>
                                    <a href="javascript:deleteTag({{$tag->id}});" class="btn btn-xs btn-danger">delete</a>
                                </div>
                            </td>
                        </tr>
                        @endforeach     
                    </tbody>
                </table>
            </div>
        </div>
    </div>   
</div>
<!-- Small modal -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">  
                {{ Form::open(array('route' => ['tags.update', $tag->id],'method' => 'put', 'class'=>'form-horizontal')) }}

                <div class="form-group">
                    {{ Form::label('modifyName', 'Name', array('class' => 'col-xs-2 control-label')) }}
                    <div class="col-xs-6 col-md-6">
                        {{ Form::text('modifyName', null, array('class'=>'form-control input-md')) }}
                    </div>                    
                </div>	              
                {{ Form::button('Close',array('class'=>'btn btn-sm btn-default col-xs-2', 'data-dismiss'=>'modal')) }}
                <div class="form-group">                    
                    <div class="col-xs-2">
                        {{ Form::submit('Save changes', array('class'=>'btn btn-sm btn-success','id'=>'tagUpdate')) }} 
                    </div>
                </div>             
                {{ Form::close() }}                
            </div>
        </div>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(document).ready(function() {
        $('#tableTags').dataTable();
    });
</script>
@stop
@stop