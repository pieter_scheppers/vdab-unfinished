<script type="text/javascript">

    function getCookie(name) {
        var re = new RegExp(name + "=([^;]+)");
        var value = re.exec(document.cookie);
        return(value != null) ? unescape(value[1]) : null;
    }
    var today = new Date();
    var expiry = new Date(today.getTime() + 30 * 24 * 3600 * 1000);
    var expired = new Date(today.getTime() - 24 * 3600 * 1000);
    function setCookie(name, value) {
        document.cookie = name + "=" + escape(value) + "; path=/; expires=" + expiry.toGMTString();
    }
    function deleteCookie(name) {
        document.cookie = name + "=null; path=/; expires=" + expired.toGMTString();
    }
    function storeValues(form)
    {
        clearCookies();
        setCookie("title", form.title.value);
        setCookie("xp", form.xp.value);
        setCookie("description", form.description.value);
        setCookie("image", form.image.value);
        $("#formAlert").slideDown(400);
        return true;
    }
    if (title = getCookie("title"))
        document.myForm.title.value = title;
    if (xp = getCookie("xp"))
        document.myForm.xp.value = xp;
    if (description = getCookie("description"))
        document.myForm.description.value = description;
    if (image = getCookie("image"))
        document.myForm.image.value = image;
    function clearCookies()
    {
        deleteCookie("title");
        deleteCookie("xp");
        deleteCookie("description");
        deleteCookie("image");
    }
    $(".alert").find(".close").on("click", function(e) {
        // Find all elements with the "alert" class, get all descendant elements with the class "close", and bind a "click" event handler
        e.stopPropagation();    // Don't allow the click to bubble up the DOM
        e.preventDefault();    // Don't let any default functionality occur (in case it's a link)
        $(this).closest(".alert").slideUp(400);    // Hide this specific Alert
    });

</script>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h1 class="page-header">Add new task</h1>

    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab1" data-toggle="tab">Basic info</a></li>
        <li><a href="#tab2" data-toggle="tab">Task Choice</a></li>
        <li><a href="#tab3" data-toggle="tab">Summary</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab1">
            <div class="alert alert-success fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                test
            </div>
            <?php include "packages/html_forms/form_task.php" ?>
            <?php include "packages/html_forms/form_subtask.php" ?>

            <form class="form-horizontal" method="post" action="">
                <span id="writeroot"></span>
                <input type="button" class="btn btn-primary" onclick="moreFields();
                        onClick();" value="Add subtask"/>
                <input type="button" id="save" class="btn btn-success" value="Save" />
            </form>
            <br/>


            <i>* optional</i> 

            <br/>
            <a class="btn btn-primary btnNext">Next</a>
        </div> <!-- /tab1 -->

        <div class="tab-pane" id="tab2">
            <a class="btn btn-primary btnPrevious">Previous</a>
            <a class="btn btn-primary btnNext">Next</a>
        </div><!-- /tab2 -->

        <div class="tab-pane" id="tab3">
            <br/>
            <div class="list-group">
                <a href="#" class="list-group-item active">
                    Task 
                </a>
                <a href="#" class="list-group-item">Title: <script type="text/javascript">document.write(getCookie("title"));</script> </a>
                <a href="#" class="list-group-item">XP: <script type="text/javascript">document.write(getCookie("xp"));</script></a>
                <a href="#" class="list-group-item">Description: <script type="text/javascript">document.write(getCookie("description"));</script></a>
                <a href="#" class="list-group-item">Image: <script type="text/javascript">document.write(getCookie("image"));</script></a>
            </div>

            <br/>
            <a class="btn btn-primary btnPrevious">Previous</a>
        </div><!-- /tab3 -->

    </div>
</div>