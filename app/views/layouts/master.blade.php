<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        {{ HTML::style('packages/bootstrap/css/bootstrap.css') }}
        {{ HTML::style('packages/bootstrap/css/bootstrap-tagsinput.css') }}
        {{ HTML::style('packages/bootstrap/css/dashboard.css') }}
        {{ HTML::style('packages/bootstrap/css/font-awesome.css') }}
        {{ HTML::style('packages/bootstrap/css/dataTables.bootstrap.css') }}
        <style type="text/css">@yield('css')</style>
        <title>@yield('title', 'VDAB Task-Panel')</title>
    </head>
    <body>
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ URL::to('/') }}">VDAB Task-Panel</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        @section('navbar')       
                        
                        <li><a href="#">Help</a></li>
                        @if(Auth::check())
                        <li><a href="{{URL::to('logout')}}">Logout</a></li>
                        @endif
                        @show
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid">		
            <div class="row">
                @section('row')
                @yield('sidebar')
                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    @if(Session::has('message'))                    
                    <div class='alert alert-{{{ null !==(Session::get('type')) ? Session::get('type') : 'info' }}}'>{{ Session::get('message')}} 
                        <button type="button" class="close" data-dismiss="alert"><span area-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    </div>
                    @endif
                    @yield('content')	
                </div>
                @show
            </div>
        </div>
        @section('scripts')
        {{ HTML::script('packages/js/jquery-1.11.1.min.js') }}
        {{ HTML::script('packages/js/bootstrap.min.js') }}
        {{ HTML::script('packages/js/jquery.bsAlerts.min.js') }}
        {{ HTML::script('packages/js/jquery.dataTables.min.js') }}
        {{ HTML::script('packages/js/dataTables.bootstrap.js') }}
        {{ HTML::script('packages/js/custom.js') }}
        {{ HTML::script('packages/js/bootstrap-tagsinput.js') }}
        {{ HTML::script('packages/js/bootstrap-tagsinput-angular.js') }}
        @show
    </body>
</html>