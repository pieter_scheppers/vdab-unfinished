@extends('layouts.master')
@section('navbar')
<li id="user" style="color: #fff"><strong>Welcome {{$firstName = Auth::user()->first_name;}} {{$lastName = Auth::user()->last_name;}} </strong></li>
<ul class="nav navbar-nav collapsed-sidebar">
    <li @if (Request::is('tasks*')) class="active" @endif >
         <a href="{{URL::to('/')}}">
            <i class="fa fa-tasks fa-fw"></i> Tasks
        </a>
    </li>
    <li @if (Request::is('tags*')) class="active" @endif>            
         <a href="{{URL::to('/tags')}}">
            <i class="fa fa-tag fa-fw"></i> Tags
        </a>
    </li>
    <li  @if (Request::is('users*')) class="active" @endif>
          <a href="{{URL::to('/users')}}">
            <i class="fa fa-users fa-fw"></i> Users
        </a>
    </li>
</ul>
@parent
@stop
@section('sidebar')
<div class="col-sm-3 col-md-2 sidebar">
    <ul class="nav nav-sidebar">
        <li @if (Request::is('tasks*')) class="active" @endif >
             <a href="{{URL::to('/')}}">
                <i class="fa fa-tasks fa-fw"></i> Tasks
            </a>
        </li>
        <li @if (Request::is('tags*')) class="active" @endif>            
             <a href="{{URL::to('/tags')}}">
                <i class="fa fa-tag fa-fw"></i> Tags
            </a>
        </li>
        <li  @if (Request::is('users*')) class="active" @endif>
              <a href="{{URL::to('/users')}}">
                <i class="fa fa-users fa-fw"></i> Users
            </a>
        </li>
    </ul>
</div>
@stop