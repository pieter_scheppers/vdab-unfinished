@extends('layouts.dashboard')
@section('content')
{{ Form::open(array('url'=>'login', 'class'=>'form-signin')) }}
<h2 class="form-signin-heading">Please Login</h2>

{{ Form::text('email', null, array('class'=>'input-block-level', 'placeholder'=>'Email Address')) }}
{{ Form::password('password', array('class'=>'input-block-level', 'placeholder'=>'Password')) }}

<div class="btn-group">

    {{ Form::submit('Login', array('class'=>'btn btn-large btn-primary'))}}
    {{ link_to('register', 'Register', array('class' => 'btn btn-large btn-default')) }}

</div>
<hr>

{{ link_to('oauthLogin/github', 'Sign in with GitHub', array('class' => 'btn btn-block btn-social btn-large btn-github')) }}
{{ link_to('oauthLogin/facebook', 'Sign in with Facebook', array('class' => 'btn btn-block btn-social btn-large btn-facebook')) }}
{{ link_to('oauthLogin/google', 'Sign in with Google', array('class' => 'btn btn-block btn-social btn-large btn-google-plus')) }}

{{ Form::close() }}
@stop