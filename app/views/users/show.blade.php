@extends('layouts.dashboard')
@section('content')
<h1 class="page-header">Modify User</h1>

<div class="panel panel-default">
    <div class="panel-heading">Modify User</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12"> 
                {{ Form::open(array('route' => ['users.update', $users->id],'method' => 'put', 'class'=>'form-horizontal')) }}
                <div class="form-group">
                    {{ Form::label('firstName', 'First name', array('class' => 'col-xs-3 control-label')) }}
                    <div class="col-xs-6 col-md-6">
                        {{ Form::text('firstName', $users->first_name , array('class'=>'form-control input-md')) }}
                    </div>                    
                </div>
                <div class="form-group">
                    {{ Form::label('lastName', 'Last name', array('class' => 'col-xs-3 control-label')) }}
                    <div class="col-xs-6 col-md-6">
                        {{ Form::text('lastName', $users->last_name, array('class'=>'form-control input-md')) }}
                    </div>                    
                </div>	
                <div class="form-group">
                    {{ Form::label('email', 'Email', array('class' => 'col-xs-3 control-label')) }}
                    <div class="col-xs-6 col-md-6">
                        {{ Form::email('email', $users->email, array('class'=>'form-control input-md')) }}
                    </div>                    
                </div>	
                <div class="form-group">
                    {{ Form::label('newPassword', 'New password*', array('class' => 'col-xs-3 control-label')) }}
                    <div class="col-xs-6 col-md-6">
                        {{ Form::password('newPassword', null, array('class'=>'form-control input-md')) }}
                    </div>                    
                </div>	
                <div class="form-group">
                    {{ Form::label('retypePassword', 'Retype password*', array('class' => 'col-xs-3 control-label')) }}
                    <div class="col-xs-6 col-md-6">
                        {{ Form::password('retypePassword', null, array('class'=>'form-control input-md')) }}
                    </div>                    
                </div>
                <span class="help-block">* is optional</span>
                {{ Form::button('Back',array('class'=>'btn btn-primary', 'onClick'=>'window.location="/users"')) }}
                {{ Form::submit('Save changes', array('class'=>'btn btn-success','id'=>'userUpdate')) }} 
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

@stop
