@extends('layouts.dashboard')
@section('content')
<div data-alerts="alerts" data-ids="myid" data-fade="4000"></div>

<h1 class="page-header">User overview</h1>

<div class="panel panel-default">
    <div class="panel-heading">Manage Users</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12"> 
                <table id="tableUsers" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach($users as $user)
                        <tr>
                            <td>{{$user->first_name}}</td>
                            <td>{{$user->last_name}}</td>
                            <td>{{$user->email}}</td>
                            <td class="text-center button"><div class="btn-group">
                                    <a href="{{ URL::to('/users/'.$user->id) }}" class="btn btn-xs btn-primary">Modify</a>
                                    <a href="javascript:deleteUser({{$user->id}});" class="btn btn-xs btn-danger">delete</a>
                                </div>
                            </td>            
                        </tr>
                        @endforeach 
                    </tbody>
                </table>
                <!-- Button trigger modal -->
                <button class="btn btn-primary" data-toggle="modal" data-target="#createUser">
                    Create new user
                </button>
            </div>
        </div>
    </div>
</div>


<!-- create user modal -->
<div class="modal fade" id="createUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Please fill in all the fields</h4>
            </div>
            {{ Form::open(array('role'=>'form', 'class'=>'form-horizontal', 'id'=>'createUser')) }}
            <div class="modal-body">
                <div class="form-group">
                    {{ Form::label('firstName', 'First name', array('class' => 'col-xs-3 control-label')) }}
                    <div class="col-xs-6 col-md-6">
                        {{ Form::text('firstName', null, array('class'=>'form-control input-md')) }}
                    </div>                    
                </div>
                <div class="form-group">
                    {{ Form::label('lastName', 'Last name', array('class' => 'col-xs-3 control-label')) }}
                    <div class="col-xs-6 col-md-6">
                        {{ Form::text('lastName', null, array('class'=>'form-control input-md')) }}
                    </div>                    
                </div>	
                <div class="form-group">
                    {{ Form::label('email', 'Email', array('class' => 'col-xs-3 control-label')) }}
                    <div class="col-xs-6 col-md-6">
                        {{ Form::email('email', null, array('class'=>'form-control input-md')) }}
                    </div>                    
                </div>	
                <div class="form-group">
                    {{ Form::label('password', 'Password', array('class' => 'col-xs-3 control-label')) }}
                    <div class="col-xs-6 col-md-6">
                        {{ Form::password('password', null, array('class'=>'form-control input-md')) }}
                    </div>                    
                </div>	
                <div class="form-group">
                    {{ Form::label('retypePassword', 'Retype password', array('class' => 'col-xs-3 control-label')) }}
                    <div class="col-xs-6 col-md-6">
                        {{ Form::password('retypePassword', null, array('class'=>'form-control input-md')) }}
                    </div>                    
                </div>	
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {{ Form::submit('Save changes', array('class'=>'btn btn-success')) }} 
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(document).ready(function() {
        $('#tableUsers').dataTable();
    });
</script>
@stop
@stop
