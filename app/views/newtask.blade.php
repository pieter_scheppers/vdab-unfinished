@extends('layouts.dashboard')
@section('content')
<h1 class="page-header">Add new task</h1>

<ul class="nav nav-tabs">
    <li class="active"><a href="#tab1" data-toggle="tab">Basic info</a></li>
    <li><a href="#tab2" data-toggle="tab">Task Choice</a></li>
    <li><a href="#tab3" data-toggle="tab">Summary</a></li>
</ul>
<div class="tab-content">
    <div class="tab-pane active" id="tab1">           
        <br/>
        {{ Form::open(array('url'=>'newtask', 'class'=>'form-horizontal','files'=> true)) }}
        <fieldset>
            <!-- title -->
            <div class="form-group">
                {{ Form::label('title', 'Title', array('class' => 'col-md-4 control-label')) }}
                <div class="col-md-4">
                    {{ Form::text('title',  Cookie::get('title'), array('class'=>'form-control input-md ')) }}
                </div>
            </div>			
            <!-- xp -->
            <div class="form-group">
                {{ Form::label('xp', 'Amount of XP', array('class' => 'col-md-4 control-label')) }}
                <div class="col-md-4">
                    {{ Form::text('xp', Cookie::get('xp'), array('class'=>'form-control input-md')) }}
                </div>
            </div>
            <!-- description -->
            <div class="form-group">
                {{ Form::label('description', 'Description', array('class' => 'col-md-4 control-label')) }}			
                <div class="col-md-4">						
                    {{ Form::textarea('description', Cookie::get('desc'), array('class'=>'form-control')) }}
                </div>
            </div>
            <!-- required task steps -->
            <div class="form-group">
                {{ Form::label('required', 'Required tasks', array('class' => 'col-md-4 control-label')) }}
                <div class="col-md-4" >			
                    {{ Form::text('required', Cookie::get('required'), array('class'=>'form-control input-md')) }}

                </div>				
                <div class="col-md-4" >		
                    {{ Form::button('Search', array('class'=>'btn btn-default', 'id'=>'task_search', 'data-toggle'=>'modal', 'data-target'=>'#myModal')) }}
                </div>

            </div>
            <br/>
            {{ Form::submit('Save Task', array('class'=>'btn btn-success'))}}
        </fieldset>
        {{ Form::close() }}

        <br/>
        <?php include "packages/html_forms/form_subtask.php" ?>

        {{ Form::open(array('url'=>'newsubtask', 'class'=>'form-horizontal','files'=> true)) }}
        <span id="writeroot"></span>
        {{ Form::button('Add subtask', array('class'=>'btn btn-primary', 'onclick'=>'moreFields();onClick();'))}}			
        {{ Form::close() }}
        <br/>      

        <a class="btn btn-primary btnNext">Next</a>
    </div> <!-- /tab1 -->

    <div class="tab-pane" id="tab2">
        <br/>
        
        <div class="panel-group" id="accordion">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            Reading task
                        </a>
                        <a href="#" data-placement="right" data-toggle="tooltip" class="tip-right" data-original-title="A reading task">
                            <i class="fa fa-question-circle"></i>
                       </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse">
                    <div class="panel-body">
                        {{ Form::open(array('url'=>'#TOFILLIN#', 'class'=>'form-horizontal')) }}

                        <fieldset>
                            <!-- choice_title -->
                            <div class="form-group">
                                {{ Form::label('title', 'Title', array('class' => 'col-md-4 control-label')) }}
                                <div class="col-md-4">
                                    {{ Form::text('title',  Cookie::get('choice_title'), array('class'=>'form-control input-md ')) }}
                                </div>
                            </div>			
                            <!-- text_to_read -->
                            <div class="form-group">
                                {{ Form::label('text_to_read', 'Text to read', array('class' => 'col-md-4 control-label')) }}	                              
                                <div class="col-md-4">						
                                    {{ Form::textarea('text_to_read', Cookie::get('choice_to_read'), array('class'=>'form-control','placeholder'=>'Fill in the text you want them to read and confirm')) }}
                                </div>
                            </div>                            
                            <br/>
                            {{ Form::submit('Save choice', array('class'=>'btn btn-success'))}}
                        </fieldset>
                        {{ Form::close() }}

                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                            Question Task
                        </a>
                    </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse">
                    <div class="panel-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                            Fill in task
                        </a>
                    </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse">
                    <div class="panel-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                </div>
            </div>
        </div>

        <br/>
        <a class="btn btn-primary btnPrevious">Previous</a>
        <a class="btn btn-primary btnNext">Next</a>
    </div><!-- /tab2 -->

    <div class="tab-pane" id="tab3">
        <br/>
        @if(Cookie::get('title') == '')
        <div class="alert alert-warning" role="alert">
            <strong>Oops!</strong> It seems like you haven't made a task yet.
        </div>
        @else
        <div class="list-group">
            <a href="#" class="list-group-item active">
                Your task
            </a>
            <a href="#" class="list-group-item"><strong>Title:</strong> 		{{ Cookie::get('title'); }}	</a>
            <a href="#" class="list-group-item"><strong>XP:</strong>			{{ Cookie::get('xp'); }}	</a>
            <a href="#" class="list-group-item"><strong>Description:</strong> 	{{ Cookie::get('desc'); }}	</a>
            <br/>
            <a href="{{ URL::route('createtask') }}" class="btn btn-success">Create Task</a>	        
        </div>
        @endif
        <br/>
        <a class="btn btn-primary btnPrevious">Previous</a>
    </div><!-- /tab3 -->

</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="searchTaskModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Search for required tasks</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Add selected task(s)</button>
            </div>
        </div>
    </div>
</div>
@stop
