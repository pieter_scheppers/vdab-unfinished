@extends('layouts.master')
@section('title', 'login')
@section('css')
body {
padding-top: 40px;
padding-bottom: 40px;
background-color: #eee;
}

.form-signin {
max-width: 330px;
padding: 15px;
margin: 0 auto;
}
.form-signin .form-signin-heading,
.form-signin .checkbox {
margin-bottom: 10px;
}
.form-signin .checkbox {
font-weight: normal;
}
.form-signin .form-control {
position: relative;
height: auto;
-webkit-box-sizing: border-box;
-moz-box-sizing: border-box;
box-sizing: border-box;
padding: 10px;
font-size: 16px;
}
.form-signin .form-control:focus {
z-index: 2;
}
.form-signin input[type="email"] {
margin-bottom: -1px;
border-bottom-right-radius: 0;
border-bottom-left-radius: 0;
}
.form-signin input[type="password"] {
margin-bottom: 10px;
border-top-left-radius: 0;
border-top-right-radius: 0;
}
@stop
@section('row')<div class="main">
    @if(Session::has('message'))
    <div class='alert alert-{{{ null !==(Session::get('type')) ? Session::get('type') : 'info' }}}'>{{ Session::get('message')}}
        <button type="button" class="close" data-dismiss="alert"><span area-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    </div>
    @endif
    {{ Form::open(array('url'=>'login', 'class'=>'form-signin')) }}
    <h2>Please sign in</h2>
    {{ Form::text('email', null, array('class'=>'form-control', 'placeholder'=>'Email Address')) }}
    {{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Password')) }}
    {{ Form::submit('Login', array('class'=>'btn btn-lg btn-primary btn-block'))}}
    {{ Form::close() }}
</div>
@stop


