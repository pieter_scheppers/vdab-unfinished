/*
 * 
 * add task menu scroll-buttons
 */
$('.btnNext').click(function() {
    $('.nav-tabs > .active').next('li').find('a').trigger('click')
            ;
});

$('.btnPrevious').click(function() {
    $('.nav-tabs > .active').prev('li').find('a').trigger('click');
});

/*
 * 
 * @returns {undefined}
 */
$('.alert .close').on('click', function(e) {
    $(this).parent().hide();
});
/*
 * Copy selected required tasks
 */
function addTasks() {
    var reqsList = document.getElementById('reqsList');

    var requiredTasks = $('.reqTask:checked').map(function() {
        return {id: this.id, title: this.value};
    }).get();
    //reqsList.innerHTML = (requiredTasks.lengh > 0) ? '' : '<i>no required tasks</i>';
    if (requiredTasks.length > 0) {
        reqsList.innerHTML = '';
    }
    else {
        reqsList.innerHTML = '<i>no required tasks</i>';
    }
    requiredTasks.forEach(function(task) {

        reqsList.innerHTML += '<a  class="list-group-item-custom removeRequired' + task['id'] + '" onClick="removeRequired(' + task['id'] + ')"><i class="fa fa-times-circle"></i>&nbsp;' + task['title'] + '</a>';

    });
}

/*
 * remove added tasks on click
 * @param {type} $number
 */
function removeRequired($number) {
    $('.reqTask#' + $number).attr('checked', false);
    $('.removeRequired' + $number).remove();
}

/*
 *	Copy values
 */
$(document).ready(function() {
    // Variables to fill
    var createTask = $('#createTask'),
            createTaskRead = $('#createTaskRead'),
            createTaskQuestion = $('#createTaskQuestion'),
            createTaskFill = $('#createTaskFill');

    // Variables to copy from
    var newTask = $('#newTask'),
            taskRead = $('#taskRead'),
            taskQuestion = $('#taskQuestion'),
            taskFill = $('#taskFill');

    // Copy all from tab1 (basic info)
    $('#copy').click(function() {
        if ($('#xp').val() == '' || $('#description').val() == '' || $('#title').val() == '') {
            warningReqFields();
        } else {
            $('#newTask').find(':input[name]').each(function() {
                $('#createTask').find(':input[name=' + this.name + ']').val(this.value);
            });
            var reqTasks = $('.reqTask:checked').map(function() {
                console.log('checked task:' + this.title);
                return {id: this.id, title: this.value};
            }).get();
            document.createTask.requiredBox.value = '';
            reqTasks.forEach(function(task) {
                document.createTask.requiredBox.value += task['title'] + '\r\n';
            });
            successSaved('basic info');
        }
    });

    //copy form elements to third tab form
    $('#btnCopyStepsForm').click(function() {
        var x = 0;
        console.log('number of steps: ' + $("#accordion").children().length);
        $('#summary-form').empty();
        $("#accordion").children().each(function() {
            var formClone = $(this).find('fieldset').clone(true);
            var inputValues = $(this).find('fieldset :input').map(function() {
                return this.value;
            }).get();

            console.log(inputValues);

            for (var i = 0; i < inputValues.length; i++) {
                if (inputValues[i] === "") {
                    x++;
                }
            }
            if (x <= 0) {
                var i = 0;
                formClone.find(':input').each(function() {
                    $(this).attr('disabled', 'true');
                    $(this).val(inputValues[i]);
                    i++;
                });
                formClone.appendTo('#summary-form');
                successSaved('task choice');
            }
            else {
                return false;
            }

            /*
             var i = 0;
             formClone.find(':input').each(function(){
             $(this).attr('disabled', 'true');
             $(this).val(inputValues[i])
             i++;
             });
             formClone.appendTo('#summary-form');*/
        });

        if (x >= 1) {
            warningReqFields();
        }
    });

    //enable fields to post variables
    jQuery(function($) {

        $('#createTask').bind('submit', function() {
            $(this).find(':input').removeAttr('disabled');
        });

    });

});

/*
 * warning required field alert
 */
function warningReqFields() {
    $(document).trigger("add-alerts", [{
            'message': "Please fill in all the required fields before saving",
            'priority': 'warning'
        }
    ]);
}

/*
 * sucess saved
 */
function successSaved($item) {
    $(document).trigger("add-alerts", [{
            'message': "Sucessfully saved " + $item,
            'priority': 'success'
        }
    ]);
}
/*
 * sucess deleted
 */
function successDeleted($item) {
    $(document).trigger("add-alerts", [{
            'message': "Sucessfully deleted " + $item,
            'priority': 'success',
        }
    ]);
}
/*
 * tooltip
 */
$(".tip-right").tooltip({
    placement: 'right'
});

/*
 * add tags
 */
$(document).ready(function() {
    var counter = 0;
    $('#addTag').click(function() {
        var array = $('#inputTag').val().split(",");
        $.each(array, function(i) {
            counter++;
            if (array[i] != '') {
                $('#selectedTags').append('<a class="list-group-item-custom removeTag' + counter + '" onClick="removeTag(' + counter + ')"><i class="fa fa-times-circle"></i>&nbsp;' + array[i] + '</a>');
            }
        });
    });
});
/*
 * remove tags
 */
function removeTag($number) {
    $('.removeTag' + $number).remove();
}

/*
 * Tasks delete workaround
 */
function deleteTask(id) {
    if (confirm('Really delete?')) {
        $.ajax({
            type: "DELETE",
            url: '/tasks/' + id,
            success: function(result) {
                location.reload();
            },
            error: function(result) {
                alert('delete failed');
            }
        });
    }
}

/*
 * Tags delete work around
 */
function deleteTag(id) {
    var request = $.ajax({
        type: 'DELETE',
        url: '/tags/' + id,
        success: function() {
            location.reload();
        }
    });
}
/*
 * Users delete work around
 */
function deleteUser(id) {
    var request = $.ajax({
        type: 'DELETE',
        url: '/users/' + id,
        success: function() {
            location.reload();
            successDeleted('user');

        }
    });
}
/*
 * display modify tag
 */
$('.tagModify').click(function() {
    var name = this.id;
    $('#modifyName').val(name);
    $('#oldValue').val(name);
});

